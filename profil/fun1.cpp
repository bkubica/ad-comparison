
#include <Interval.h>
#include <IntervalVector.h>
#include <IntervalMatrix.h>
#include <AutoDiff/IntervalAutoDiff.h>
#include <NLF/NLF.h>
#include <Functions.h>
#include <Constants.h>
#include <Utilities.h>

#include "gen.hpp"


using namespace std;


INTERVAL_AUTODIFF fun1 (CONST INTERVAL_AUTODIFF & x) {
	return Sqr(x(1)) + Sin(x(2));// + Max(x(1), 0.0); 
}

FUNCTION Func (2, fun1);


int main() {
	INTERVAL_VECTOR x(2);
	RandomIntervalGenerator rig(-10.0, 10.0);
	const size_t num_points{1000000};
	for (size_t step = 0; step < num_points; ++step) {
		for (int i = 1; i <= 2; ++i) x(i) = rig.next_interval<INTERVAL>();
		//cout << "x = " << x << "\n";

		//INTERVAL y = FunctionValue(fun1(x));
		INTERVAL_VECTOR grad = GradientValue (fun1(x));
		//INTERVAL_MATRIX hesse = HessianValue (fun1(x));

		//cout << "y = " << y << "\n";
	}
	return 0;
}

