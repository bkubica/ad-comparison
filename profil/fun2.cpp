
#include <Interval.h>
#include <IntervalVector.h>
#include <IntervalMatrix.h>
#include <AutoDiff/IntervalAutoDiff.h>
#include <NLF/NLF.h>
#include <Functions.h>
#include <Constants.h>
#include <Utilities.h>

#include "gen.hpp"


using namespace std;


INTERVAL_AUTODIFF fun1 (CONST INTERVAL_AUTODIFF & x) {
	//cout << x.ComputeHessian << "\n";
	INTERVAL_AUTODIFF result1, result2;
	result1 = Sqr(x(1));
	result2 = Cos(x(1));
	for (int i = 2; i <= Dimension(x); ++i) {
		result1 = result1 + Sqr(x(i));
		result2 = result2 * Cos(x(i));
	}
	return result1 + result2;
}

FUNCTION Func (2, fun1);


int main() {
	const int n = 10;
	INTERVAL_VECTOR x(n);
	RandomIntervalGenerator rig(-10.0, 10.0);
	const size_t num_points{1000000};
	//cout << INTERVAL_AUTODIFF::ComputeHessian << "\n";
	for (size_t step = 0; step < num_points; ++step) {
		for (int i = 1; i <= n; ++i) x(i) = rig.next_interval<INTERVAL>();
		//cout << "x = " << x << "\n";

		//INTERVAL y = FunctionValue(fun1(x));
		INTERVAL_VECTOR grad = GradientValue (fun1(x));
		//INTERVAL_MATRIX hesse = HessianValue (fun1(x));

		//cout << "y = " << y << "\n";
	}
	return 0;
}

