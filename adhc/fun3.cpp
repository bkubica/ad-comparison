
#include <adhc.hpp>

#include "gen.hpp"


using namespace std;
using namespace adhc;


template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> fun(const adhc_vector<lev, sparse_mode, n> &x) {
	adhc_ari<lev, sparse_mode, n> result1, result2, result3;;
	result1 = max(x[1], 0.0);
	result2 = x[1];
	result3 = x[1];
	for (int i = 2; i <= n; ++i) {
		result1 += max(x[i], 0.0);
		result2 += x[i];
		result3 *= x[i];
	}
	return result1 + max(result2, result3);
}


int main() {
	const int lev = 1;
	const int n = 10;
	cxsc::ivector x(n);
	RandomIntervalGenerator rig(-10.0, 10.0);
	const size_t num_points{1000000};
	for (size_t step = 0; step < num_points; ++step) {
		for (int i = 1; i <= n; ++i) x[i] = rig.next_interval<cxsc::interval>();
		//cout << "x = " << x << "\n";

		adhc_vector<lev, SparsityLevel::highly_sparse, n> x_{x};
		adhc_ari<lev, SparsityLevel::highly_sparse, n> y_ = fun(x_);

		//cout << "y_ = " << y_ << "\n";
	}
	return 0;
}

