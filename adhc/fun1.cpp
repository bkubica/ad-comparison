
#include <adhc.hpp>

#include "gen.hpp"


using namespace std;
using namespace adhc;


template<int lev, SparsityLevel sparse_mode, int n>
adhc_ari<lev, sparse_mode, n> fun(const adhc_vector<lev, sparse_mode, n> &x) {
	return sqr(x[1]) + sin(x[2]);// + Max(x(1), 0.0); 
}


int main() {
	const int lev = 1;
	cxsc::ivector x(2);
	RandomIntervalGenerator rig(-10.0, 10.0);
	const size_t num_points{1000000};
	for (size_t step = 0; step < num_points; ++step) {
		for (int i = 1; i <= 2; ++i) x[i] = rig.next_interval<cxsc::interval>();
		//cout << "x = " << x << "\n";

		adhc_vector<lev, SparsityLevel::highly_sparse, 2> x_{x};
		adhc_ari<lev, SparsityLevel::highly_sparse, 2> y_ = fun(x_);

		//cout << "y_ = " << y_ << "\n";
	}
	return 0;
}

