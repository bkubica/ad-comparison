Benchmarks to compare AD codes of **ADHC** and the old C-XSC, PROFIL/BIAS and IBEX.
They are used in the paper *Algorithmic differentiation and hull-consistency enforcing using C++ template meta-programming* in **Journal of Numerical Algorithms**.

IBEX programs are run in Docker containers.


To create the containers, please run:

    cd ibex_gaol/
    docker build -t ibex-gaol-image ./
    docker run -it --name ibex-gaol ibex-gaol-image

Then in the container, you can:

    cd AD-comparison-gaol

and run 

    ./fun1
    ./fun2
or
    ./fun3

possibly modifying their codes and recompiling by:

    make


For other directories: ibex_bias and ibex_filib, the procedure is analogous.

