#include "ibex.h"
#include "gen.hpp"

using namespace std;
using namespace ibex;

int main(int argc, char** argv) {
	const int n = 10;
	Variable x(n, "x");
	const ExprNode *e1 = &(sqr(x[0]));
	const ExprNode *e2 = &(cos(x[0]));
	for (int i = 1; i < n; ++i) {
		e1 = & (*e1 + sqr(x[i]));
		e2 = & (*e2 * cos(x[i]));
	}
	Function fun(x,*e1 + *e2, "fun");
	//cout << f << endl;

	IntervalVector box(n);
	RandomIntervalGenerator rig(-10.0, 10.0);
	const size_t num_points{1000000};
	for (size_t step = 0; step < num_points; ++step) {
		for (int i = 0; i < n; ++i) box[i] = rig.next_interval<Interval>();
		//cout << "x = " << box << "\n";
		//Interval y = fun.eval(box);
		IntervalVector grad = fun.gradient(box);
		//cout << "y = " << y << "\n";
	}
	return 0;
}

