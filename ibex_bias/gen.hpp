#include <random>


struct RandomIntervalGenerator {
	std::mt19937_64 gen;
	std::uniform_real_distribution<> distr;

	RandomIntervalGenerator(const double xl, const double xh) {
		std::random_device dev;
		gen = std::mt19937_64(dev());
		distr = std::uniform_real_distribution<> (xl, xh);
	}

	template<typename T>
	T next_interval() {
		double a = distr(gen);
		double b = distr(gen);
		if (a <= b) return T(a, b);
		else return T(b, a);
	}
};

