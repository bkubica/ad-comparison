#include "ibex.h"
#include "gen.hpp"

using namespace std;
using namespace ibex;

int main(int argc, char** argv) {
	const int n = 10;
	Variable x(n, "x");
	const ExprNode *e1 = &(max(x[0], 0.0));
	const ExprNode *e2 = &(x[0]);
	const ExprNode *e3 = &(x[0]);
	for (int i = 1; i < n; ++i) {
		e1 = & (*e1 + max(x[i], 0.0));
		e2 = & (*e2 + x[i]);
		e3 = & (*e3 * x[i]);
	}
	Function fun(x,*e1 + max(*e2, *e3), "fun");
	cout << fun << endl;

	IntervalVector box(n);
	RandomIntervalGenerator rig(-10.0, 10.0);
	const size_t num_points{1000000};
	for (size_t step = 0; step < num_points; ++step) {
		for (int i = 0; i < n; ++i) box[i] = rig.next_interval<Interval>();
		//cout << "x = " << box << "\n";
		//Interval y = fun.eval(box);
		IntervalVector grad = fun.gradient(box);
		//cout << "y = " << y << "\n";
	}
	return 0;
}

