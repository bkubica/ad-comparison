#include "ibex.h"
#include "gen.hpp"

using namespace std;
using namespace ibex;

int main(int argc, char** argv) {
	Variable x(2, "x");
	Function fun(x, sqr(x[0]) + sin(x[1]));// + max(x[0], 0.0));
	//cout << fun << endl;

	IntervalVector box(2);
	RandomIntervalGenerator rig(-10.0, 10.0);
	const size_t num_points{1000000};
	for (size_t step = 0; step < num_points; ++step) {
		for (int i = 0; i < 2; ++i) box[i] = rig.next_interval<Interval>();
		//cout << "x = " << box << "\n";
		//Interval y = fun.eval(box);
		IntervalVector grad = fun.gradient(box);
		//cout << "y = " << y << "\n";
	}
	return 0;
}

