
#include <ivector.hpp>
#include <hess_ari.hpp>

#include "gen.hpp"


using namespace std;


HessType fun1(const HTvector &x) {
	HessType result1, result2;
	result1 = sqr(x[1]);
	result2 = cos(x[1]);
	for (int i = 2; i <= x.Dim(); ++i) {
		result1 = result1 + sqr(x[i]);
		result2 = result2 * cos(x[i]);
	}
	return result1 + result2;
}


int main() {
	const int n = 10;
	cxsc::ivector x(n);
	RandomIntervalGenerator rig(-10.0, 10.0);
	const size_t num_points{1000000};
	for (size_t step = 0; step < num_points; ++step) {
		for (int i = 1; i <= n; ++i) x[i] = rig.next_interval<cxsc::interval>();
		//cout << "x = " << x << "\n";

		cxsc::interval y;
		//fEvalH(fun1, x, y);
		cxsc::ivector grad;
		fgEvalH(fun1, x, y, grad);
		cxsc::imatrix Hesse;
		//fghEvalH(fun1, x, y, grad, Hesse);

		//cout << "y_ = " << y_ << "\n";
	}
	return 0;
}

