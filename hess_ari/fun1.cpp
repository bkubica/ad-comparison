
#include <ivector.hpp>
#include <hess_ari.hpp>

#include "gen.hpp"


using namespace std;


HessType fun1(const HTvector &x) {
	return sqr(x[1]) + sin(x[2]);// + max(x[1], 0.0); 
}


int main() {
	const int lev = 1;
	cxsc::ivector x(2);
	RandomIntervalGenerator rig(-10.0, 10.0);
	const size_t num_points{1000000};
	for (size_t step = 0; step < num_points; ++step) {
		for (int i = 1; i <= 2; ++i) x[i] = rig.next_interval<cxsc::interval>();
		//cout << "x = " << x << "\n";

		//adhc_vector<lev, SparsityLevel::highly_sparse, 2> x_{x};
		//adhc_ari<lev, SparsityLevel::highly_sparse, 2> y_ = fun1(x_);
		cxsc::interval y;
		cxsc::ivector grad;
		fgEvalH(fun1, x, y, grad);

		//cout << "y_ = " << y_ << "\n";
	}
	return 0;
}

